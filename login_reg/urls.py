from django.conf.urls import url, include
from login_reg.views import *

urlpatterns = [
    url(r'^reg-page/$', registration_page, name='reg-page'),
    url(r'^login-page/$', login_page, name='login-page'),
    url(r'^registration-user/$', register_user, name='registration-user'),
    url(r'^login-user/$', login_user, name='login-user'),
    url(r'^dashboard-page/$', dashboard, name='dashboard-page'),
    url(r'^export-csv/$', export_csv, name='export-csv'),
    url(r'^logout-user/$', logout_user, name='logout-user'),

]