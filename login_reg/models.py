# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserPermissions(models.Model):

    user = models.OneToOneField(User)
    count = models.IntegerField(default=0)
    blocking_time = models.DateTimeField(null=True)

class CandidateData(models.Model):

    serial_num = models.CharField(max_length=100, null=True)
    resume = models.BooleanField(default=False)
    name = models.CharField(max_length=100, null=True)
    mobile_num = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=300, null=True)
    work_exp = models.CharField(max_length=100, null=True)
    current_location = models.CharField(max_length=100, null=True)
    nearest_city = models.CharField(max_length=100, null=True)
    pref_location = models.CharField(max_length=100, null=True)
    ctc = models.CharField(max_length=100, null=True)
    current_employer = models.CharField(max_length=200, null=True)
    current_designation = models.CharField(max_length=200, null=True)
    skills = models.CharField(max_length=5000, null=True)
    ugc = models.CharField(max_length=200, null=True)
    ugc_college_name = models.CharField(max_length=200, null=True)
    ug_passing_year = models.CharField(max_length=200, null=True)
    trier_1 = models.CharField(max_length=20, null=True)
    pgc = models.CharField(max_length=200, null=True)
    pgc_college_name = models.CharField(max_length=200, null=True)
    pg_passing_year = models.CharField(max_length=200, null=True)
