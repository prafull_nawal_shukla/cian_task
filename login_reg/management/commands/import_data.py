from django.core.management.base import BaseCommand
from login_reg.views import import_candidate_data

class Command(BaseCommand):
    """

    """
    args = ''
    help = 'Used For Importing Candidate Data'

    def handle(self, *args, **options):
        """

        :param args:
        :param options:
        :return:
        """
        self.stdout.write("===> Operation Start ===>")

        import_candidate_data()

        self.stdout.write("===> Operation Done ===>")
