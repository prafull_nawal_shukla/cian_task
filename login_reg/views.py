# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, render_to_response
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.utils import timezone
from login_reg.models import *
from django.contrib.auth import authenticate, login, logout
import itertools

# Create your views here.

data_header_field = ['sr_num', 'resume', 'name', 'mo_num', 'email', 'work_exp', 'current_loc', 'nearest_city',
                     'pref_loc',
                     'ctc', 'current_employer', 'current_designation', 'skills', 'ugc', 'ugc_college', 'trier_1',
                     'ugc_passing_year', 'pgc',
                     'pgc_college', 'pgc_passing_year']


def registration_page(request):
    try:
        return render(request, 'register.html')
    except Exception as e:
        print(e)


def login_page(request):
    try:
        return render(request, 'login.html')
    except Exception as e:
        print(e)


def register_user(request):
    try:
        name = request.POST['name'].strip().split(' ')
        first_name = name[0]
        last_name = " ".join(name[1:])
        email = request.POST['email'].strip()
        password = request.POST['password'].strip()

        if not User.objects.filter(username=email).exists():
            user = User.objects.create(first_name=first_name, last_name=last_name,
                                       email=email, username=email, password=password)
            user.set_password(password)
            user.save()
            messages.success(request, 'User Register Successfully. Login now!')
            return redirect('login-page')
        else:
            messages.warning(request, 'User Already register with Us.')
            return redirect('reg-page')

    except Exception as e:
        messages.warning(request, 'Something Bad happened Please contact Administrator.')
        return redirect('reg-page')


def login_user(request):
    try:
        user_name = request.POST['email'].strip()
        password = request.POST['password'].strip()
        if user_name and password is not None:
            user_obj = User.objects.filter(username=user_name)
            if user_obj:
                user = authenticate(username=user_name, password=password)
                if user is not None:
                    login(request, user)
                    return redirect('dashboard-page')
                    # return render(request, 'dashboard.html',
                    #               {'f_name': user.first_name, 'l_name': user.last_name})
                else:
                    messages.warning(request, 'Username or password is incorrect.')
                    return redirect('login-page')

            else:
                messages.warning(request, 'User does not exists.')
                return redirect('login-page')
        else:
            messages.warning(request, 'Please Enter User name and Password.')
            return redirect('login-page')
    except Exception as e:
        messages.warning(request, 'Something Bad happened Please contact Administrator.')
        return redirect('login-page')


@login_required(login_url='/')
def dashboard(request):
    try:
        filter_data = []
        current_location_sel = None
        work_exp_sel = None
        skills_sel = None
        if request.method == 'POST':
            try:
                user_per_obj = UserPermissions.objects.filter(user_id=request.user.id)
                if user_per_obj:
                    user_per_obj = user_per_obj.first()
                    if user_per_obj.blocking_time.date == timezone.now().date():
                        count = user_per_obj.count
                        count += 1
                        user_per_obj.count = count
                        user_per_obj.save()
                    else:
                        user_per_obj.count = 1
                        user_per_obj.blocking_time = timezone.now()
                        user_per_obj.save()
                else:
                    user_per_obj = UserPermissions.objects.create(user_id=request.user.id, blocking_time=timezone.now(),
                                                                  count=1)
                if user_per_obj.count > 5:
                    messages.warning(request, 'Daily limit of getting data from filter exceed')
                    return redirect('dashboard-page')
                else:
                    work_exp = request.POST['we']
                    current_location = request.POST['cl']
                    skill = request.POST['sk']
                    c_objs = CandidateData.objects.filter(work_exp=work_exp, current_location=current_location,
                                                          skills__icontains=skill)
                    filter_data = itertools.imap(
                        lambda z: returning_cand_dict(z), iter(c_objs))
                    current_location_sel = current_location
                    work_exp_sel = work_exp
                    skills_sel = skill
            except Exception as e:
                filter_data = []
        cand_obj = CandidateData.objects.all()
        current_location = set(cand_obj.values_list('current_location', flat=True))
        work_exp = set(cand_obj.values_list('work_exp', flat=True))
        skills = list(cand_obj.values_list('skills', flat=True))
        list_skill = list()
        for skill in skills:
            list_skill.extend(skill.split(','))
        return render(request, 'dashboard.html',
                      {'current_location': current_location, 'work_exp': work_exp, 'skills': set(list_skill),
                       'filter_data': filter_data, 'current_location_sel': current_location_sel,
                       'work_exp_sel': work_exp_sel, 'skills_sel': skills_sel})

    except Exception as e:
        print e


def returning_cand_dict(data):
    try:
        data = {'serial_num': data.serial_num,
                'resume': data.resume, 'name': data.name, 'mobile_num': data.mobile_num,
                'email': data.email, 'work_exp': data.work_exp, 'current_location': data.current_location,
                'nearest_city': data.nearest_city, 'pref_location': data.pref_location, 'ctc': data.ctc,
                'current_employer': data.current_employer, 'current_designation': data.current_designation,
                'skills': data.skills, 'ugc': data.ugc, 'ugc_college_name': data.ugc_college_name,
                'ug_passing_year': data.ug_passing_year, 'trier_1': data.trier_1, 'pgc': data.pgc,
                'pgc_college_name': data.pgc_college_name, 'pg_passing_year': data.pg_passing_year}
        return data
    except:
        pass


def export_csv(request):
    try:
        work_exp = request.POST['we']
        current_location = request.POST['cl']
        skill = request.POST['sk']
        c_objs = CandidateData.objects.filter(work_exp=work_exp, current_location=current_location,
                                              skills__icontains=skill)
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=candidate_data.csv'

        myFields = ['Serial Num', 'Resume', 'Name', 'Mobile Num', 'Email', 'Working Exp', 'Current Location',
                    'Nearest City', 'Pref Location', 'CTC', 'Current Employer', 'Current Designation', 'Skills',
                    'UG Course Name', 'UG College Name', 'UG Passing Year', 'Trier_1', 'PG College Name',
                    'PG Course Name', 'PG Passing Year']
        writer = csv.DictWriter(response, fieldnames=myFields)
        writer.writeheader()
        for i in c_objs:
            writer.writerow({'Serial Num': i.serial_num,
                             'Resume': i.resume, 'Name': i.name, 'Mobile Num': i.mobile_num,
                             'Email': i.email, 'Working Exp': i.work_exp, 'Current Location': i.current_location,
                             'Nearest City': i.nearest_city, 'Pref Location': i.pref_location, 'CTC': i.ctc,
                             'Current Employer': i.current_employer, 'Current Designation': i.current_designation,
                             'Skills': i.skills, 'UG Course Name': i.ugc, 'UG College Name': i.ugc_college_name,
                             'UG Passing Year': i.ug_passing_year, 'Trier_1': i.trier_1, 'PG Course Name': i.pgc,
                             'PG College Name': i.pgc_college_name, 'PG Passing Year': i.pg_passing_year})

        return response
    except:
        pass


def create_candidate_data_import(*args):
    try:
        serial_num = args[0][0]
        resume = args[0][1]
        name = args[0][2]
        mobile_num = args[0][3]
        email = args[0][4]
        work_exp = args[0][5]
        current_location = args[0][6]
        nearest_city = args[0][7]
        pref_location = args[0][8]
        ctc = args[0][9]
        current_employer = args[0][10]
        current_designation = args[0][11]
        skills = args[0][12]
        ugc = args[0][13]
        ugc_college_name = args[0][14]
        trier_1 = args[0][15]
        ug_passing_year = args[0][16]
        pgc = args[0][17]
        pgc_college_name = args[0][18]
        pg_passing_year = args[0][19]
        if resume.lower() == 'yes':
            resume = True
        else:
            resume = False
        CandidateData.objects.create(serial_num=serial_num, resume=resume, name=name, mobile_num=mobile_num,
                                     email=email, work_exp=work_exp, current_location=current_location,
                                     nearest_city=nearest_city, pref_location=pref_location, ctc=ctc,
                                     current_employer=current_employer, current_designation=current_designation,
                                     skills=skills, ugc=ugc, ugc_college_name=ugc_college_name, trier_1=trier_1,
                                     ug_passing_year=ug_passing_year, pgc=pgc, pgc_college_name=pgc_college_name,
                                     pg_passing_year=pg_passing_year)
        return True, 'Done'
    except Exception as e:
        return False, e


import csv


def import_candidate_data():
    try:
        with open('cian_task/sample_data.csv') as f:
            reader = csv.reader(f)
            count = 0
            for row in reader:
                count += 1
                if count == 1:
                    if row == data_header_field:
                        continue
                    else:
                        print('Headers are incorrect')
                else:
                    done, error = create_candidate_data_import(row)
                    if not done:
                        print('Something bad happened in importing data.')
            print('Successfully imported.')
        pass
    except Exception as e:
        print(e)

def logout_user(request):
    logout(request)
    try:
        request.user.auth_token.delete()
        return redirect('login-page')
    except:
        return redirect('login-page')